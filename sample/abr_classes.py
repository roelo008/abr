"""
Tool core components, including calculations etc
Hans Roelofsen, 27-07-2021

NAAMGEVING

ABR:    abiotische randvoorwaarden
ABR_DB: abiotische randvoorwaarden database. Samengesteld uit 2 Excels. De ABR_DB rijen zijn plantensoorten OF
        habitattypen (gezamenlijk aangeduid als UNITS). De ABR_DB kolommen zijn percentielen voor ABIOTISCHE CONDITIES
        ie ABIOTICS.

Tegenover de ABR_DB staan MEASUREMENTS. Hiermee wordt bedoeld veldwaarnemingen van 1 of meer ABIOTISCHE CONDITIES.
Elke rij van de MEASUREMENTS is een meetlocatie, ook wel MEASUREMENT of FEATURE. De kolommen van de MEASUREMENTS zijn
1 of meer ABIOTICS.
"""

import os
import datetime
import numpy as np
import pandas as pd
import utils

# from sample import utils
from shapely.geometry import point
import geopandas as gp


# TODO: abr.query_full en abr.query_simple kunnen waarschijnlijk wel gecombineerd worden


class Sampling:
    """
    Simple class for holding relations between quantiles and associated probabilites and booleans. Method for returning
    probability or boolean for given quantile as follows:

    Sided-testing
    single-right Prob  1.0   1.0           1.0           0.5   0.1
    single-left  Prob  0.1   0.5           1.0           1.0   1.0
    double       Prob  0.1   0.5           1.0           0.5   0.1
    single-right bool   T     T             T             T     F
    single-left  bool   F     T             T             T     T
    double       bool   F     T             T             T     F
                      -----|-----|----------*----------|-----|-----
    Quantiel               5    25                    75    95
    Qindex              0     1             2             3     4

    NOTE!
    Right: gemeten waarde kan alleen TE HOOG zijn, maar nooit TE LAAG
    Left: gemeten waarde kan alleen TE LAAG zijn, maar nooit TE HOOG
    """

    def __init__(self):
        self.q_names = [
            "x < q05",
            "q05 <= x < q25",
            "q25 <= x < q75",
            "q75 <= x < q95",
            "x >= q950",
        ]
        self.q_index = [0, 1, 2, 3, 4]
        self.right_prob = [1, 1, 1, 0.5, 0.1]
        self.left_prob = [0.1, 0.5, 1, 1, 1]
        self.double_prob = [0.1, 0.5, 1, 0.5, 0.1]
        self.right_bool = [True, True, True, True, False]
        self.left_bool = [False, True, True, True, True]
        self.double_bool = [False, True, True, True, False]

    def query(self, q, sided="double-sided", of="prob", **kwargs):
        """
        Given a quantiel, return a probability or boolean. See schematic above

        :param q: quantiel indicator, either index or named (str)
        :param sided: perform either both-sided or one-sided testing. one-sided is either left or right sided,
                      depending on abiotic.
        :param of: output format, either "prob" or "bool"
        :param kwargs: name of abiotic if side == one-sided
        :return: probability or boolean related to a quantile
        """

        if sided == "one-sided":
            try:
                which_side = utils.sampling_strategy(kwargs.get("abiotic"))
            except KeyError:
                print(
                    "! error: provide valid abiotic name when chosing one-sided testing"
                )
                raise
        else:
            which_side = "right"  # dummy value

        keys = getattr(
            self, {str: "q_names", int: "q_index", np.int64: "q_index"}[type(q)]
        )
        vals = {
            "double-sided": getattr(self, "double_{0}".format(of)),
            "one-sided": getattr(self, "{0}_{1}".format(which_side, of)),
        }[sided]
        d = dict(zip(keys, vals))
        try:
            return d[q]
        except KeyError:
            print(
                "provide valid quanitile indicator, either int 0-4 or description, but not {}".format(
                    q
                )
            )
            raise


class Measurements:
    """
    Class to hold everything related to an datasource with field measurements of abiotic conditions
    """

    def __init__(self, io, **kwargs):
        """
        :param io: valid string path to *.csv, *.txt, *.xlsx
        """

        assert os.path.isfile(io), "{0} is not a valid data source".format(io)
        assert os.path.splitext(io)[1] in [
            ".xlsx",
            ".txt",
            ".csv",
        ], "datasource can be *.xlsx, *.txt or *.csv"

        self.io = io
        self.spatial = False

        try:
            if os.path.splitext(io)[1] == ".xlsx":
                self.db = pd.read_excel(
                    self.io, sheet_name=kwargs["sheet"], index_col=kwargs["id"]
                )
                self.sheet = kwargs.get("sheet", None)
            else:
                self.db = pd.read_csv(
                    self.io, sep=kwargs["sep"], index_col=kwargs["id"]
                )
            self.id_col = kwargs.get("id", None)
        except KeyError as e:
            print("Please provide keyword argument: {0}".format(str(e)))
            raise

        # Which abiotics are provided in this requestor dataset?
        self.abiotics = list(set(list(self.db)).intersection(utils.abiotics(of="set")))
        assert len(self.abiotics) > 0, "No valid abiotics found in datasource"

        # Look for something like coordinates. If found, convert to GeoDataFrame
        x_col = [
            i for i in ["x", "X", "easting", "Easting", "GK-R"] if i in self.db.columns
        ]
        y_col = [
            i
            for i in ["y", "Y", "northing", "Northing", "GK-H"]
            if i in self.db.columns
        ]
        if (
            len(x_col) == 1
            and self.db[x_col[0]].dtype.kind in "biufc"
            and len(y_col) == 1
            and self.db[y_col[0]].dtype.kind in "biufc"
            and all(self.db.loc[:, [x_col[0], y_col[0]]].notna())
        ):
            x, y = x_col[0], y_col[0]
            print("Identified {0} and {1} as coordinate columns".format(x, y))
            self.db = gp.GeoDataFrame(
                data=self.db,
                geometry=[point.Point(a, b) for a, b in zip(self.db[x], self.db[y])],
            )
            self.spatial = True

    def get_info(self, id, col):
        pass
        # TODO

    def to_gpkg(self, gpkg: str, lyr: str, df):
        """
        write shapefile to disk with everyting from df appended
        :param gpkg: path to geopackage
        :param lyr: layer name
        :param df:
        :return:
        """

        assert self.spatial, "cannot write shapefile, non-geospatial measurements"
        out = pd.merge(
            left=self.db, right=df, how="left", left_index=True, right_index=True
        )
        print("  caution, defaulting to CRS epsg:5677")
        out.set_crs(
            epsg=5677, inplace=True
        )  # Hard coded to Wilhelmshaven CRS. Should be variable later
        out.drop("Datum", axis=1).to_file(gpkg, layer=lyr, driver="GPKG")


class Abr:
    """
    Holder for the abiotic conditions database
    """

    def __init__(self, out_dir, basename):
        self.out_dir = out_dir
        self.excel_out = None

        self.db = utils.read_abr()

        self.abiotics = utils.abiotics()
        self.project_name = basename  # TODO: variable
        self.ts = datetime.datetime.now().strftime("%d-%b-%Y_%H:%M:%S")
        self.ts_simple = datetime.datetime.now().strftime("%Y%m%dt%H%M")

        self.luronAT2Latin = dict(
            zip(self.db.luronAT, self.db.Latin)
        )  # mapping species Abbreviation 2 Latin name
        self.Latin2luronAT = dict(zip(self.db.Latin, self.db.luronAT))  # inverse

        self.Dutch2Latin = dict(
            zip(self.db.Dutch, self.db.Latin)
        )  # mapping between local name and latin name
        self.Latin2Dutch = dict(zip(self.db.Latin, self.db.Dutch))  # inverse

        self.luronAT2Dutch = dict(zip(self.db.luronAT, self.db.Dutch))
        self.Dutch2luronAT = dict(zip(self.db.Dutch, self.db.luronAT))

        self.SpecNr2Dutch = dict(zip(self.db.SpecNr, self.db.Dutch))
        self.Dutch2SpecNr = dict(zip(self.db.Dutch, self.db.SpecNr))

        self.Dutch2what = dict(zip(self.db.Dutch, self.db.what))
        self.Dutch2Dutch = dict(zip(self.db.Dutch, self.db.Dutch))

        self.Dutch2English = dict(zip(self.db.Dutch, self.db.English))
        self.English2Dutch = dict(zip(self.db.English, self.db.Dutch))

        self.sampling = Sampling()

        self.blacklisted = False

    def valid_unit(self, unit_request):
        """
        Is the requested unit known in the ABR databe as either a Local Name, Latin Name, Species Nr or Abbreviation?
        If so, return database index, else False
        :param unit_request: unit name
        :return: index or boolean
        """

        for col in ["luronAT", "English", "Latin", "Dutch", "SpecNr"]:
            q = self.db.query("{0} == '{1}'".format(col, unit_request))
            if not q.empty:
                return col, q.index
        raise TypeError(
            "Error: {} is not recognized in the ABR database".format(unit_request)
        )

    def blacklist(self, **kwargs):
        """
        Read blacklisting information from file and remove selected items from self.db
        :param kwargs: all arguments provided to ABR
        :return: updated self.db
        """

        # map between keyword arguments from MRT argparser and the keyword arguments in utils.read_blacklist
        argkeys = {
            "bl_io": "io",
            "bl_id": "id_col",
            "bl_sep": "sep",
            "bl_sheet": "sheet",
            "bl_filter_col": "filter_col",
            "bl_true_val": "filter_true",
            "bl_type": "unit_type",
        }
        blkwargs = {}
        for k, v in kwargs.items():
            try:
                blkwargs[argkeys[k]] = v
            except KeyError:
                pass

        # Pass only relevant and renamed arguments to function to read blacklist
        blacklist, unit_type = utils.read_blacklist(**blkwargs)

        # Identify items that match blacklisted items
        in_blacklist = self.db.loc[self.db[unit_type].isin(blacklist)].index
        print(
            "dropping {0} {1}s from ABR database due to blacklisting".format(
                len(in_blacklist), unit_type
            )
        )
        self.db.drop(in_blacklist, inplace=True)

        self.blacklisted = True

    def query_simple(
        self,
        measurement_df,
        abiotics,
        what="both",
        how="4_measurement",
        sided="double-sided",
        key="Dutch",
        verbose=False,
    ):
        """
        For all measurements in a measurement df, match to all units in the ABR database
        :param measurement_df: df with abiotic measurements (features in rows, abiotics in cols, feature ID as index)
        :param abiotics: list of abiotics present in measurement_df
        :param what: plants, habitats or both
        :param how: output format, dataframe rows are either measurements ('by_measurement') or units ('by_units')
        :param sided: sampling strategy
        :param key: reporting key
        :return: dataframe with all measurement X unit combinations

        Iterate over 3 levels!

        measurements
                     \ units
                              \ abiotics
        """

        # Use which units from the ABR database?
        abr_indx = {
            "plant": self.db.loc[self.db.what == "plant"].index,
            "habitat": self.db.loc[self.db.what == "habitat"].index,
            "both": self.db.index,
        }[what]

        # output dataframe
        out = pd.DataFrame(
            index=self.db.loc[abr_indx, key], columns=measurement_df.index
        )

        # iterate over the features in the measurement dataframe
        for measurement in measurement_df.index:

            if verbose:
                print("  doing {}".format(measurement))

            # Series holding for a MEASUREMENT outcomes for all UNITS
            measurement_outcome = pd.Series(index=self.db.loc[abr_indx, key])

            # Iterate over requested units in the ABR database. Using Dutch name, but could also be something other
            for unit in self.db.loc[abr_indx, key]:

                # empty lost to hold all abiotic matches for this measurement X unit combination
                unit_outcome = []

                # iterate over the abiotic conditions
                for abiotic in abiotics:

                    # In ABR db identify row corresponding to requested unit and columns corresponding to abiotic
                    abr_target = np.squeeze(
                        np.array(
                            self.db.loc[
                                self.db[key] == unit,
                                (self.db.columns.str.startswith(abiotic))
                                & ~(self.db.columns.str.endswith("mean")),
                            ]
                        )
                    )

                    # If all NaN appnes NaN to outcomes and continue
                    if np.isnan(abr_target).all():
                        unit_outcome.append(np.nan)
                        continue

                    # Assert only 4 values are extracted and are ascending
                    assert abr_target.shape == (
                        4,
                    ), "Problem with extracting from ABR database."
                    assert np.array_equal(
                        np.sort(abr_target), abr_target
                    ), "ABR are not ascending: {}".format(abr_target)

                    # In measurement df, identify row and measured abiotic value
                    measured_val = measurement_df.loc[measurement, abiotic]

                    if np.isnan(measured_val):
                        continue

                    # Measured value lies in which quantile?
                    quantile = utils.within_range(
                        measured_val, abr_target, of="q_index"
                    )

                    # Test quantile for boolean pass
                    outcome = self.sampling.query(
                        q=quantile, of="bool", sided=sided, abiotic=abiotic
                    )
                    unit_outcome.append(outcome)

                # One Out All Out Principe
                measurement_outcome[unit] = all(unit_outcome)

            out[measurement] = measurement_outcome

        return {"4_measurement": out.T, "4_unit": out}[how]

    def query_full(
        self,
        measurement_df,
        abiotics,
        id,
        what="both",
        how="4_measurement",
        sided="double-sided",
        **kwargs
    ):
        """
        Query either all measurements for a single unit (4_unit)
        or all all units for a single measurement (4_measurement) and provide elaborate output.
        :param how: str 4_measurement (Default) or 4_unit
        :param measurement_df: dataframe with all measurements data
        :param abiotics: list of abiotics
        :param id: depends on *how*
                             4_measurement - index from measurements dataframe for which to run all units
                             4_unit        - Latin, Dutch, SpecNr or Abrr for one unit from the ABR database
        :param what: choose (plants, habitat, both)
        :param how: calculate for a single measurement or for a single unit
        :param sided: choose (double, left, right) for testing type. See utils.quantiel2probability
        :param of: output format full or top. For latter, specify top n in kwargs
        :return:
            4_measurement: dataframe with units as rows and all results for the measurement as columns
            4_unit:        dataframe with measurements as rows and all results for the unit as columns
        """

        # print('  doing full query for {}'.format(id))

        # Use which units from the ABR database?
        abr_indx = {
            "plant": self.db.loc[self.db.what == "plant"].index,
            "habitat": self.db.loc[self.db.what == "habitat"].index,
            "both": self.db.index,
        }[what]

        # TARGET   = a single measurement if 4_measurement
        #            a single unit if 4_unit
        target = id

        # If 4_unit, verify that requested unit exists and translate to Dutch name
        if how == "4_unit":
            unittype, _ = self.valid_unit(target)

            # Cast to integer if SpecNr provided. May already be int, but can't be sure
            if unittype == "SpecNr":
                target = int(target)

            # Translate to Dutch name using <type>2Dutch dictionary
            target = getattr(self, "{}2Dutch".format(unittype))[target]

        # holder for results
        out = []

        # ITERABLE = all units if 4_measurement (identified by Dutch name)
        #            all measurements if 4_unit
        try:
            iterable = {
                "4_measurement": self.db.loc[abr_indx, "Dutch"],
                "4_unit": measurement_df.index,
            }[how]
        except KeyError:
            print("Select either 4_measurement or 4_unit as method")
            raise

        for x in iterable:

            # Generate result placeholder
            results = utils.result_series(abiotics=abiotics, name=x)
            n, limiting = 0, 0

            # Target name is either from iterable or a unit from ABR
            target_name = {"4_measurement": x, "4_unit": target}[how]

            # Get identifier of the measurement, either the consistent target (if 4_measurement) or from iterable
            measurement_identifier = {"4_measurement": target, "4_unit": x}[how]

            # Iterate over all abiotics
            for abiotic in abiotics:

                # Get measured value for this abiotic. Continue to next abiotic if not available
                measured_val = measurement_df.loc[measurement_identifier, abiotic]
                if np.isnan(measured_val):
                    continue

                # Get target ABR values, either for a single consistent unit (4_unit) or for x in iterable
                abr_target = np.squeeze(
                    np.array(
                        self.db.loc[
                            self.db.Dutch == target_name,
                            (self.db.columns.str.startswith(abiotic))
                            & ~(self.db.columns.str.endswith("mean")),
                        ]
                    )
                )

                # If target array is filled with NaN, then this abiotic is not quantified for this unit
                if np.isnan(abr_target).all():
                    results.loc["{}_value".format(abiotic)] = measured_val
                    continue

                # Assert only 4 ascending values are extracted
                assert abr_target.shape == (
                    4,
                ), "Problem with extracting from ABR database."
                assert np.array_equal(
                    np.sort(abr_target), abr_target
                ), "ABR are not ascending: {}".format(abr_target)

                # match measurement against ABR
                quantile = utils.within_range(measured_val, abr_target, of="q_name")

                # probabilty of occurrence for given measurement against ABR and boolean
                occ_prob = self.sampling.query(
                    q=quantile, of="prob", sided=sided, abiotic=abiotic
                )
                within = self.sampling.query(
                    q=quantile, of="bool", sided=sided, abiotic=abiotic
                )
                limiting += 0 if within else 1

                # plug into results series
                results.loc["{}_value".format(abiotic)] = measured_val
                results.loc["{}_quantile".format(abiotic)] = quantile
                results.loc["{}_probability".format(abiotic)] = occ_prob
                results.loc["{}_within".format(abiotic)] = within

                # Valid abiotics plus one
                n += 1

            results["Latin"] = self.Dutch2Latin[target_name]
            results["Dutch"] = target_name
            results["SpecNr"] = self.Dutch2SpecNr[target_name]
            results["luronAT"] = self.Dutch2luronAT[target_name]
            results["English"] = self.Dutch2English[target_name]
            results["what"] = self.Dutch2what[target_name]
            results["n"] = n
            results["n_limiting"] = limiting
            # Kans op voorkomen is product van de kansen van elk abiotische randvoorwaarden
            results["overall_prob"] = results.loc[
                results.index.str.endswith("probability")
            ].product()
            # Eerste abiotiek met de laagste kans
            results["most_limiting"] = (
                results.loc[results.index.str.endswith("probability")]
                .astype(float)
                .idxmin()
            )
            # Units is passed voor de measurement als alle Abiotiek binnen de randvoorwaarden vallen
            results["passed"] = all(results.loc[results.index.str.endswith("within")])
            out.append(results)

        return {
            "4_unit": pd.concat(out, axis=1),
            "4_measurement": pd.concat(out, axis=1).T,
        }[how]

    def query_top(self, n, full_query, identifier, of="full"):
        """
        report n units with greatest occurence probability per measurement
        :return: dataframe columns: |measurement-ID|Dutch|SpecNr|Score(1,2,3,4,5)|Prob|
                              rows: habitattypen with n highest probabilities
        """

        # Get full query, ie output from self.query_full
        all4measurement = full_query

        # Gather all probabilites and store in descending array
        probs_descending = np.sort(
            np.array(list(set(all4measurement.overall_prob.astype(float))))
        )[::-1]

        # Map highest score to 1 etc.
        prob2score = dict((j, i) for i, j in enumerate(probs_descending, start=1))

        # Add scores as column and measurement ID as column
        all4measurement["score"] = all4measurement.overall_prob.map(prob2score)
        all4measurement["ID"] = identifier

        # Return n highest scores (ie score 1, 2, 3... n) and selection of columns
        return {
            "full": all4measurement.loc[
                all4measurement.score <= n,
                ["ID", "Dutch", "English", "SpecNr", "score", "overall_prob", "passed"],
            ].sort_values(by=["score"], ascending=True),
            "average": all4measurement.loc[
                all4measurement.score <= n, "overall_prob"
            ].mean(),
        }[of]

    def stats_per_measurement(self, full_query, identifier, n):
        """
        Report on several fun statistics per measurement
        :param full_query: output of self.query_full
        :param identifier: str
        :return: pandasseries
        """

        return pd.Series(
            {
                "max_prob": full_query.overall_prob.max(),
                "max_prob_unit": full_query.overall_prob.astype(float).idxmax(),
                "max_prob_unit_English": self.Dutch2English[
                    full_query.overall_prob.astype(float).idxmax()
                ],
                "max_prob_SpecNr": full_query.loc[
                    full_query.overall_prob.astype(float).idxmax(), "SpecNr"
                ],
                "avg_prob": full_query.overall_prob.mean(),
                "n_true": full_query.passed.sum(),
                "avg_prob_top{}".format(n): self.query_top(
                    n, full_query, identifier, "average"
                ),
                #'easting': 0,
                #'northing': 0}
            },
            name=identifier,
        )

    def full_per_measurement(self, full_query, identifier):
        pass

    def generate_xls(self, out_dir="./", **kwargs):
        """
        Generate report with metadata sheet
        :param basename
        :param out_dir: output directory
        :param kwargs: write all kwargs as metadata items.
        :return: Excel document on disk
        """

        self.excel_out = "Report_{0}_{1}.xlsx".format(self.project_name, self.ts_simple)
        with pd.ExcelWriter(
            os.path.join(self.out_dir, self.excel_out), mode="w"
        ) as writer:

            # Report on input
            sp_path, hab_path = utils.abr_path()
            metadata = {
                "ABR_Species_database": sp_path,
                "ABR_Species_database_created": utils.file_timestamps(
                    sp_path, "created"
                ),
                "ABR_Species_database_last_modified": utils.file_timestamps(
                    sp_path, "modified"
                ),
                "ABR_Habitat_database": hab_path,
                "ABR_Habitat_database_created": utils.file_timestamps(
                    hab_path, "created"
                ),
                "ABR_Habitat_database_modified": utils.file_timestamps(
                    hab_path, "modified"
                ),
                "report creation_date": self.ts,
                "created_by": "{0}@{1}".format(
                    os.getenv("USERNAME"), os.getenv("COMPUTERNAME")
                ),
            }
            for k, v in kwargs.items():
                # Map keyword arguments to neat metadatakeys
                neat_keys = utils.arguments2metadata()
                metadata[neat_keys[k]] = v
            pd.DataFrame(data=metadata, index=[0]).T.to_excel(
                writer, sheet_name="Metadata", index=True
            )

    def write2sheet(self, sheet_name, df, **kwargs):
        """
        Add sheet with dataframe to an existing Excel
        :param sheet_name: duh...
        :param excel_io: exisiting Excel
        :param df: pandas dataframe to wriote
        :return: None
        """

        if not self.excel_out:
            raise TypeError("Create output excel first")

        with pd.ExcelWriter(
            os.path.join(self.out_dir, self.excel_out), mode="a"
        ) as writer:
            df.to_excel(writer, sheet_name=sheet_name, **kwargs)
