"""
utility functions for ABR tool
Hans Roelofsen 27-07-2021
"""

import os
import itertools
import datetime
import numpy as np
import pandas as pd


def abr_path():
    """
    :return: path to abr database if present on local PC, else AssertionError
    """

    abr_path = r"c:\Users\{0}\Wageningen University & Research\Wilhemshaven - General\e_randvoorwaarden\input".format(
        os.environ.get("username")
    )
    if os.path.isdir(abr_path):
        return os.path.join(
            abr_path, "abiotische randvoorwaarden soorten 2019.xlsx"
        ), os.path.join(abr_path, "habitattypen.xlsx")
    else:
        raise AssertionError(
            "Cannot connect to ABR database. Please ensure Wilhemshaven Teams site is mapped to "
            "this computer."
        )


def file_timestamps(io, what="created"):
    """
    return datetime string when file at io was last modified
    :param io: valid  file path
    :return: datetiem string
    """

    if os.path.isfile(io):
        return {
            "modified": datetime.datetime.fromtimestamp(os.path.getmtime(io)).strftime(
                "%d-%b-%Y_%H:%M:%S"
            ),
            "created": datetime.datetime.fromtimestamp(os.path.getctime(io)).strftime(
                "%d-%b-%Y_%H:%M:%S"
            ),
        }[what]
    else:
        raise ValueError("{} does not exist".format(io))


def colname_remap():
    return {
        "C_N_curve_corr_C_050": "C_N_corr_C_050",
        "C_N_curve_corr_C_250": "C_N_corr_C_250",
        "C_N_curve_corr_C_750": "C_N_corr_C_750",
        "C_N_curve_corr_C_950": "C_N_corr_C_950",
        "Ca_Curve_corr_C_050": "Ca_corr_C_050",
        "Ca_Curve_corr_C_250": "Ca_corr_C_250",
        "Ca_Curve_corr_C_750": "Ca_corr_C_750",
        "Ca_Curve_corr_C_950": "Ca_corr_C_950",
        "ghg_Curve_corr_C_050": "GHG_corr_C_050",
        "ghg_Curve_corr_C_250": "GHG_corr_C_250",
        "ghg_Curve_corr_C_750": "GHG_corr_C_750",
        "ghg_Curve_corr_C_950": "GHG_corr_C_950",
        "glg_Curve_corr_C_050": "GLG_corr_C_050",
        "glg_Curve_corr_C_250": "GLG_corr_C_250",
        "glg_Curve_corr_C_750": "GLG_corr_C_750",
        "glg_Curve_corr_C_950": "GLG_corr_C_950",
        "gvg_Curve_corr_C_050": "GVG_corr_C_050",
        "gvg_Curve_corr_C_250": "GVG_corr_C_250",
        "gvg_Curve_corr_C_750": "GVG_corr_C_750",
        "gvg_Curve_corr_C_950": "GVG_corr_C_950",
        "K_uit_Curve_corr_C_050": "K_uit_corr_C_050",
        "K_uit_Curve_corr_C_250": "K_uit_corr_C_250",
        "K_uit_Curve_corr_C_750": "K_uit_corr_C_750",
        "K_uit_Curve_corr_C_950": "K_uit_corr_C_950",
        "NH4_Curve_corr_C_050": "NH4_CaCl2_corr_C_050",
        "NH4_Curve_corr_C_250": "NH4_CaCl2_corr_C_250",
        "NH4_Curve_corr_C_750": "NH4_CaCl2_corr_C_750",
        "NH4_Curve_corr_C_950": "NH4_CaCl2_corr_C_950",
        "NO3_Curve_corr_C_050": "NO3_CaCl2_corr_C_050",
        "NO3_Curve_corr_C_250": "NO3_CaCl2_corr_C_250",
        "NO3_Curve_corr_C_750": "NO3_CaCl2_corr_C_750",
        "NO3_Curve_corr_C_950": "NO3_CaCl2_corr_C_950",
        "Ntot_Curve_corr_C_050": "Ntot_corr_C_050",
        "Ntot_Curve_corr_C_250": "Ntot_corr_C_250",
        "Ntot_Curve_corr_C_750": "Ntot_corr_C_750",
        "Ntot_Curve_corr_C_950": "Ntot_corr_C_950",
        "pH_Curve_corr4_C_050": "pH_corr_C_050",
        "pH_Curve_corr4_C_250": "pH_corr_C_250",
        "pH_Curve_corr4_C_750": "pH_corr_C_750",
        "pH_Curve_corr4_C_950": "pH_corr_C_950",
        "Ptot_Curve_corr_C_050": "Ptot_corr_C_050",
        "Ptot_Curve_corr_C_250": "Ptot_corr_C_250",
        "Ptot_Curve_corr_C_750": "Ptot_corr_C_750",
        "Ptot_Curve_corr_C_950": "Ptot_corr_C_950",
        "vocht_curve_corr_lin_C_050": "vocht_corr_C_050",
        "vocht_curve_corr_lin_C_250": "vocht_corr_C_250",
        "vocht_curve_corr_lin_C_750": "vocht_corr_C_750",
        "vocht_curve_corr_lin_C_950": "vocht_corr_C_950",
    }


def read_abr():
    """
    Read ABR databse for plant species
    :return: pandas data frame
    """
    try:
        sp_path, hab_path = abr_path()
        sp = pd.read_excel(sp_path, sheet_name="abiotische_randvoorwaarden_soor")
        hab = pd.read_excel(
            hab_path,
            sheet_name="habitattypen",
            usecols=lambda x: False if x == "Short" else True,
        )

        # Remap species colnames
        sp.rename(columns=colname_remap(), inplace=True)

        # Concatenate
        db = pd.concat(
            [sp.assign(what="plant"), hab.assign(what="habitat")],
            axis=0,
            ignore_index=True,
        )

        # Assert integrity between Local Name, Scientific Name, Species Nr and Abbreviations
        assert (
            len(set(zip(db.Dutch, db.Latin))) == db.shape[0]
        ), "Invalid coupling between Local and Scientific Names"
        assert (
            len(set(zip(db.luronAT, db.Dutch))) == db.shape[0]
        ), "Invalid coupling btwn Abbreviation and Local Name"
        assert (
            len(set(zip(db.SpecNr, db.Dutch))) == db.shape[0]
        ), "Invalid coupling btwn SpecNr and Local Name"

        return db

    except PermissionError as e:
        print("Cannot read opened Excel: {}.".format(str(e)[47:]))
        raise


def abiotics(of="list"):
    """
    :return: list or set of abiotic factors
    """
    abiotics = [
        "C_N",
        "Ca",
        "Cl",
        "GHG",
        "GLG",
        "GVG",
        "K",
        "Mg",
        "NH4",
        "NO3",
        "Ntot",
        "pH",
        "Ptot",
        "vocht",
    ]
    return {"list": abiotics, "set": set(abiotics)}[of]


def within_range(x, arr, of="Simple"):
    """
    Compare value X to 5, 25, 50, 75, 95 percentielen
    :param x: float
    :param arr: numpy array (4,)
    :param of: output format Simple or Full
    :param sided:
    :return: if Simple: boolean x lies between min/max of arr
             if Full:   0 x < arr[0]
                        1 arr[0] <= x < arr[1]
                        2 arr[1] <= x < arr[2]
                        3 arr[2] <= x < arr[3]
                        4 arr[3] <= x
    """
    if x == np.nan:
        return x
    else:
        return {
            "Simple": (x >= arr.min()) & (x <= arr.max()),
            "q_index": np.digitize(x, arr),
            "q_name": {
                0: "x < q05",
                1: "q05 <= x < q25",
                2: "q25 <= x < q75",
                3: "q75 <= x < q95",
                4: "x >= q950",
            }[np.digitize(x, arr)],
        }[of]


def result_series(abiotics, name):
    """
    create empty named series to hold all results
    :param abtiocs: list
    :return: pd Series
    """

    template = ["value", "quantile", "probability", "within"]
    abiotic_templates = [
        "{0}_{1}".format(a, b) for (a, b) in itertools.product(abiotics, template)
    ]

    return pd.Series(
        index=[
            "Latin",
            "Dutch",
            "English",
            "SpecNr",
            "luronAT",
            "what",
            "passed",
            "overall_prob",
            "n_limiting",
            "n",
            "most_limiting",
        ]
        + abiotic_templates,
        name=name,
        dtype=object
    )


def progress_bar(
    iterable, prefix="", suffix="", decimals=1, length=100, fill="#", printEnd="\r"
):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    credits: https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console
    """
    total = len(iterable)

    def printProgressBar(iteration):
        percent = ("{0:." + str(decimals) + "f}").format(
            100 * (iteration / float(total))
        )
        filledLength = int(length * iteration // total)
        bar = fill * filledLength + "-" * (length - filledLength)
        print("\r{0} |{1}| {2}% {3}".format(prefix, bar, percent, suffix), end=printEnd)

    # Initial Call
    printProgressBar(0)

    # Update Progress Bar
    for i, item in enumerate(iterable):
        yield item
        printProgressBar(i + 1)
    # Print New Line on Complete
    print()


def sampling_strategy(abiotic):
    """
    return appropriate sampling strategy depending on abiotic
    :param abiotic: str
    :return: double, left or right
    """

    d = {
        "C_N": "left",
        "Ca": "left",
        "Cl": "right",
        "GHG": "right",
        "GLG": "right",
        "GVG": "right",
        "K": "right",
        "Mg": "left",
        "NH4": "right",
        "NO3": "right",
        "Ntot": "right",
        "pH": "left",
        "Ptot": "right",
        "vocht": "left",
    }

    return d[abiotic]


def read_blacklist(io, id_col, unit_type, verbose=True, **kwargs):
    """
    Read file from disk with list of plantspecies and/or habitattypes to ignore in further analysis
    :param io: file path to either *.csv, *.txt or *.xlsx
    :param id_col: column to use
    :param unit_type: blacklist item are English names, Dutch names, Latin names or species numbers?
    :param verbose: boolean, report on progress
    :param kwargs: optional sheetname if src.endswith('xlsx')
    :return:
    """
    assert os.path.isfile(io), "{0} is not a valid data source".format(io)
    assert os.path.splitext(io)[1] in [
        ".xlsx",
        ".txt",
        ".csv",
    ], "datasource can be *.xlsx, *.txt or *.csv"
    valid_types = ["Latin", "Dutch", "English", "luronAT", "SpecNr"]
    assert unit_type in valid_types, "invalid type {0}, must be in {1}".format(
        type, valid_types
    )

    use_cols = [id_col]
    if "filter_col" in kwargs:
        use_cols.append(kwargs["filter_col"])

    try:
        if os.path.splitext(io)[1] == ".xlsx":
            db = pd.read_excel(io, sheet_name=kwargs["sheet"], usecols=use_cols)
        else:
            db = pd.read_csv(io, sep=kwargs["sep"], usecols=use_cols)
    except KeyError as e:
        print("Please provide keyword argument: {0}".format(str(e)))
        raise
    assert len(set(db[id_col])) == db.shape[0], "duplicates found"

    if "filter_col" in kwargs and "filter_true" in kwargs:
        bl_matches = db.loc[db[kwargs["filter_col"]] == kwargs["filter_true"], :].index
    else:
        bl_matches = db.index

    if verbose:
        print(
            "openend {0} for {1} type blacklist, with {2} blacklisted items.".format(
                io, unit_type, len(bl_matches)
            )
        )

    return db.loc[bl_matches, id_col], unit_type


def arguments2metadata():
    """ "
    :return dict: mapping between argparse arguments (shorthand) to verbose metadata keys
    """

    return {
        "measured": "path to measurement data",
        "id": "ID column in measurement data",
        "sheet": "Excel sheet containing measurement data",
        "sep": "column seperator",
        "what": "analysis contains habitat, plants or both?",
        "sampling": "sampling strategy",
        "summary": "report contains summary?",
        "top": "report contains top outcomes?",
        "n": "how many top outcomes?",
        "full": "report contains full analysis?",
        "basename": "report basename",
        "out_dir": "report output directory",
        "summstats": "report contains summary statistics?",
        "blacklist": "blacklist has been applied?",
        "bl_io": "blacklist source data file",
        "bl_id": "blacklist source data column",
        "bl_sep": "seperator",
        "bl_sheet": "Excel sheet name",
        "bl_filter_col": "second column in blacklist data source for filtering",
        "bl_true_val": "value in filter column indicating blacklist items",
        "bl_type": "blacklist data file contains English, Dutch, Latin names or species nr?",
        "report_key": "reporting key",
        "verbose": "verbose",
        "geo": "write measurements as Shapefile",
    }


# foo, t = read_blacklist(io=r'c:\Users\roelo008\Wageningen University & Research\Wilhemshaven - General\e_randvoorwaarden\opzet\selectie habitat typen voor Wilhelmshaven.xlsx',
#                      id_col='complete', unit_type='English', sheet='Sheet1', filter_col='target type', filter_true='no')
