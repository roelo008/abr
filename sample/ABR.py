"""
User interface for ABR tool
Hans Roelofsen, 27-07-2021
"""

import os
import sys

# from sample import abr_classes as abrc
import abr_classes as abrc
import pandas as pd
import argparse

parser = argparse.ArgumentParser()
# Compulsary arguments
parser.add_argument(
    "measured", help="path to xlsx, csv or txt with soil sample data", type=str
)
parser.add_argument("id", help="name of column with sample identifiers", type=str)

# Optional arguments
parser.add_argument("--sheet", help="Excel sheet name, if needed.", type=str)
parser.add_argument("--sep", help="column seperator for csv files", type=str)
parser.add_argument(
    "--what",
    help="plants, habitattypen or both?",
    type=str,
    default="both",
    choices=["plant", "habitat", "both"],
)
parser.add_argument(
    "--sampling",
    help="sampling strategy",
    type=str,
    choices=["double-sided", "one-sided"],
    default="double-sided",
)
parser.add_argument("--summary", help="write summary to report", action="store_true")
parser.add_argument(
    "--summstats", help="write summary stats per measurement", action="store_true"
)
parser.add_argument("--full", help="write full query to report", action="store_true")
parser.add_argument(
    "--report_key",
    help="unit reporting key",
    choices=["English", "Dutch", "Latin", "Speciesnr"],
    default="Dutch",
)
parser.add_argument(
    "--basename", help="output report basename", type=str, default="WILHELMSHAVEN"
)
parser.add_argument("--out_dir", help="output directory", type=str, default="./")
parser.add_argument("--verbose", help="detailed feedback", action="store_true")

# parser group for top n outcomes
group1 = parser.add_argument_group("top")
group1.add_argument("--top", help="write top-n to report", action="store_true")
group1.add_argument("--n", help="how many top results?", type=int, default=5)

# parser group for blacklisting
group2 = parser.add_argument_group("blacklisting")
group2.add_argument("--blacklist", help="Enable blacklisting", action="store_true")
group2.add_argument("--bl_io", help="file path to blacklist", type=str)
group2.add_argument("--bl_id", help="ID column", type=str)
group2.add_argument("--bl_sep", help="column separator", type=str)
group2.add_argument("--bl_sheet", help="Excel sheet", type=str)
group2.add_argument("--bl_filter_col", help="Filter column name", type=str)
group2.add_argument("--bl_true_val", help="true values in filter col", type=str)
group2.add_argument(
    "--bl_type",
    help="blacklist contains what?",
    type=str,
    choices=["Latin", "Dutch", "English", "luronAT", "SpecNr"],
)

# parser group for writing geospatial output
group3 = parser.add_argument_group("geospatial")
group3.add_argument("--geo", help="write shapefile output", action="store_true")

# parse arguments
args = parser.parse_args()

try:
    print("Starting")

    # Initiate Abiotische randvoorwaarden database
    ww = abrc.Abr(out_dir=args.out_dir, basename=args.basename)

    # If blacklisting is requested, pass all arguments as a dictionary to the method
    if args.blacklist:
        ww.blacklist(**vars(args))

    # Initiate measurements class
    measurements = abrc.Measurements(
        io=args.measured, sep=";", id=args.id, sheet=args.sheet
    )

    # Generate output Excel with all arguments as metadata
    ww.generate_xls(**vars(args))

    if args.summary:
        print("Doing summaries")
        short_by_measurement = ww.query_simple(
            measurement_df=measurements.db,
            abiotics=measurements.abiotics,
            how="4_measurement",
            what=args.what,
            sided=args.sampling,
            key=args.report_key,
            verbose=args.verbose,
        )
        ww.write2sheet(sheet_name="summary_by_measurement", df=short_by_measurement)
        ww.write2sheet(sheet_name="summary_by_unit", df=short_by_measurement.T)

    if any([args.top, args.full, args.summstats, args.geo]):
        print("Doing full queries")
        holder1, holder2, holder3 = [], [], []

        # Iterate over measurements
        for m in measurements.db.index:
            print("  {}".format(m))
            out = ww.query_full(
                measurement_df=measurements.db,
                abiotics=measurements.abiotics,
                how="4_measurement",
                id=m,
                what=args.what,
                sided=args.sampling,
            )
            if args.full:
                ww.write2sheet(sheet_name="{}".format(m), df=out, index=False)
            if args.top:
                holder1.append(ww.query_top(n=args.n, full_query=out, identifier=m))
            if args.summstats:
                holder2.append(
                    ww.stats_per_measurement(full_query=out, identifier=m, n=args.n)
                )
            if args.geo:
                holder3.append(out.assign(id=m))

        if args.top:
            print("Writing top {}".format(args.n))
            df = pd.concat(holder1)
            ww.write2sheet(
                sheet_name="top{}_by_measurement".format(args.n), df=df, index=False
            )

        if args.summstats:
            df = pd.concat(holder2, axis=1).T
            ww.write2sheet(sheet_name="Summary Stats", df=df, index=True)

        if args.geo:
            df = pd.concat(holder3).astype(
                {"overall_prob": float, "n": int, "n_limiting": int}
            )
            # n = pd.pivot_table(df, index='id', columns=args.key, values='n', aggfunc='mean')
            # n_limiting = pd.pivot_table(df, index='id', columns=args.key, values='n_limiting', aggfunc='mean')
            # most_limiting = pd.pivot_table(df, index='id', columns=args.key, values='most_limiting', aggfunc='first')
            gpkg_destination = os.path.join(
                args.out_dir,
                "Geodata_{0}_{1}.gpkg".format(ww.project_name, ww.ts_simple),
            )
            lyr_destination = "sample_locations_habitattype_probability"
            overall_prob = pd.pivot_table(
                data=df,
                index="id",
                columns=args.report_key,
                values="overall_prob",
                aggfunc="mean",
            )
            measurements.to_gpkg(
                gpkg=gpkg_destination, lyr=lyr_destination, df=overall_prob
            )

    print("...done")
    sys.exit(0)

except (KeyError, ValueError, AssertionError, PermissionError) as e:
    print("|----------------------------------------------------|")
    print(e)
    print("|----------------------------------------------------|")
    sys.exit(0)
