# Abiotic Boundary Conditions tool
by [Hans Roelofsen](https://www.wur.nl/nl/Personen/Hans-dr.-HD-Hans-Roelofsen.htm) & [Wieger Wamelink](https://www.wur.nl/en/Persons/Wieger-dr.ir.-GWW-Wieger-Wamelink.htm). Wageningen Environmental Research, august 2021. 

## Introduction
The ABR tool was developed by Wageningen Environmental Research Institute in august 2021. Development aim was to leverage the pre-existing database of abiotic boundary conditions (ABR database) and to simply and quickly assess how well a plant species or habitat type would fit in an area characterized by abiotic condition measurements.   

For backwards-compatability, the Dutch-acronym ABR (for 'Abiotische Randvoorwaarden') will be used in this document and in the program-code.

## Theory
The ABR tool is designed to expose measured or estimated abiotic (soil) conditions towards known boundary conditions of plant species and/or [Habitat Types](https://www.natura2000.nl/profielen/habitattypen), thereby assessing if measured/estimated conditions are within the viability range of the target species/type.      

A species/type is considered viable towards a measurement/estimate of an abiotic condition if the latter: 

* lies between the species/type lower- and upper viability limits of the same abiotic condition **OR** 
* is not less than the species/type lower viability limit **OR** 
* is not in excess of the species/type upper viability limit.

Testing for the upper- and lower limits simultaniously is dubbed `double-sided` testing. `Single-sided` testing considers only the lower- OR the upper limit and is thus further specified as:

* `single-sided-left`: the species/type never experiences over-abundance of the abiotic condition, e.g. nutrient shortage but never excess is detrimental to the species occurrence probability. 
* `single-sided-right`: the species/type can never suffer from experiencing too little/few of the abtiotic condition, e.g.   

Testing either `double-sided` or `single-sided` depends on research goals and context. When testing single-sided, the abiotic condition in question dictates if `single-sided-left` or `single-sided-right` is applied (see [here](https://git.wur.nl/roelo008/abr/-/blob/master/sample/utils.py#L274).)   

The upper- and lower limits are provided in the ABR database in the form of 5, 25, 75 and 95 quantile values of the abiotic condition at which the species/type has been found to occur. The 5 and 95 quanitile values are typically used as lower- and upper limits, although the 25 and 75 quantiles could also be used. 

Slotting a measurement/estimate into the quantile ranges yields a boolean presence/absence indication as follows:

```
    Quantile                   5    25                    75    95
                          -----|-----|----------|----------|-----|-----

    Presence/Absence
      single-left         False True          True          True  True
      single-right        True  True          True          True  False
      double              False True          True          True  False
```
Alternatively, an occurrence probability can be drawn from the ABR database, as follows:
```
    Quantile                   5    25                    75    95
                          -----|-----|----------|----------|-----|-----
    
    Probability
      single-left          0.1   0.5           1.0           1.0   1.0
      single-right         1.0   1.0           1.0           0.5   0.1
      double               0.1   0.5           1.0           0.5   0.1
```

The ABR database is not part of this repository, but stored and managed separately on WEnR premises.  

## Installation
The ABR tool is designed in the [Python](https://docs.conda.io/en/latest/miniconda.html) programming language, using several non-default modules. The ABR source code is stored as a WUR Gitlab repository and managed using [GIT](https://git-scm.com/). To install the ABR tool on your machine: 

1. install [git](https://git-scm.com/), see [Getting Started - Installing GIT](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).
2. install [miniconda](https://docs.conda.io/en/latest/miniconda.html)
3. using git ([see the docs](https://git-scm.com/docs/git-clone)), clone the ABR repository from the WUR gitlab to a local directory, e.g. `c:\tools\abr`
4. using miniconda, create a new python environment ([see the docs](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-with-commands)) and install the following modules:
    * python 3.9 
    * [pandas](https://pandas.pydata.org/) 1.1.5 or later
    * [numpy](https://numpy.org/) 1.19.4 or later
    * [openpyxl](https://openpyxl.readthedocs.io/en/stable/) 3.0.5 or later
5. in Command Prompt, [activate](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#activating-an-environment) the conda environment
6. in Command Prompot, navigate to the `sample` directory in the local ABR installation directory, e.g. `c:/tools/abr/sample` 
7. type `python sample/abr.py --help` to invoke the ABR help menu.     

## Input
An Excel (xlsx) or comma-separated table (*.csv or *.txt) where rows are sample features and columns are measurement values of one or more abiotic conditions. The following abiotic conditions are currently supported and may be offered in the table columns.

|description|unit|columnname|
|-----|-----|-----|
C/N ratio| [-] |C_N
Calcium in water solution| mg/kg |Ca
Chloride content |mg/kg|Cl
Average Groundwatertable in winter|cm below surface level|GHG
Average Groundwatertable in summer|cm below surface level|GLG
Average Groundwatertable in spring|cm below surface level|GVG
Kalium in water solution|mg/kg|K
Magnesium in water solution|mg/kg|Mg
Ammonium in CaCl<sub>2</sub> extract|mg/kg|NH4
Nitrate in CaCl<sub>2</sub> extract|mg/kg|NO3
Total nitrogen content|mg/kg|Ntot
pH|-|pH
Total Phosporus content|mg/kg|Ptot
moisture content|%|vocht

Alongside the abiotic condition columns, a column must be dedicated to identifying each sample with a unique string or number.  

## Output
The tool generates an Excel document with content depending on user instructions. Firstly, the user specifies if the samples should be exposed towards just plant species, habitat types or both (hereafter referred to as `units`). Secondly, the user must opt for `single-sided` or `double-sided` testing. Finally, the analysis may be reported on from three different perspectives:   

* `summary`: Boolean `True` or `False` for all measurement-unit combinations. Presented in two tabs; once with the measurements as rows and units as columns ('by measurement') and once reversed ('by unit').    
* `top`: tab with top `n` units per measurement and corresponding occurrence probabilities. 
* `full`: tab per measurement detailing the viability of each unit. Specifies the score of each abiotic condition, as well as the total number of applicable abiotics and number of limiting abiotics.
* `summstats`: single tab with summary data per measurement, containing among other data the highest scoring unit, the average occurrence probability and number of viable units. 

In addition, a metadata tab is generated by default in which the date, operator, source file and settings are recorded.


## Usage
The ABR tool considers two positional (and therefore compulsary), several optional arguments (starting with `--`) and three argument groups. An argument group is invoked by providing its first argument (e.g. `--top`), followed by the subsequest parameter group arguments.

```
usage: abr.py [-h] [--sheet SHEET] [--sep SEP] [--what {plant,habitat,both}] [--sampling {double-sided,one-sided}]
              [--summary] [--summstats] [--full] [--report_key {English,Dutch,Latin,Speciesnr}] [--basename BASENAME]
              [--out_dir OUT_DIR] [--verbose] [--top] [--n N] [--blacklist] [--bl_io BL_IO] [--bl_id BL_ID]
              [--bl_sep BL_SEP] [--bl_sheet BL_SHEET] [--bl_filter_col BL_FILTER_COL] [--bl_true_val BL_TRUE_VAL]
              [--bl_type BL_TYPE] [--geo]
              measured id

positional arguments:
  measured              path to xlsx, csv or txt with soil sample data.
  id                    name of column with sample identifiers.

optional arguments:
  -h, --help            show this help message and exit
  --sheet SHEET         Excel sheet name, if <measured> is *.xlsx
  --sep SEP             column seperator, if <measured> is *.txt or *.csc
  --what {plant,habitat,both}
                        plants, habitattypen or both?
  --sampling {double-sided,one-sided}
                        sampling strategy
  --summary             write summary to report
  --summstats           write summary stats per measurement
  --full                write full query to report
  --report_key {English,Dutch,Latin,Speciesnr}
                        unit reporting key. 
  --basename BASENAME   output report basename
  --out_dir OUT_DIR     output directory
  --verbose             detailed feedback

top:
  --top                 write top-n to report
  --n N                 how many top results?

blacklisting:
  --blacklist           enable blacklisting
  --bl_io BL_IO         file path to blacklist
  --bl_id BL_ID         name of columns with identifiers for blacklisting
  --bl_sep BL_SEP       column separator, if <bl_io> is *.txt or *.csv
  --bl_sheet BL_SHEET   Excel sheet name, if <bl_io> is *.xlsx\
  --bl_filter_col BL_FILTER_COL
                        column name for identifying units to blacklist 
  --bl_true_val BL_TRUE_VAL
                        value in <bl_filter_col> that identifies unit to blacklist
  --bl_type BL_TYPE     typology of identifiers in <bl_io>-<bl-id>, must be either: 'Latin', 'Dutch', 'English', 'luronAT', 'SpecNr'. 

geospatial:
  --geo                 write shapefile output
```

For example:

`python abr.py c:/data/metingen.xlsx sample_name --sheet final --what both --summary --top --n 3 --basename TESTRUN --out_dir c:/tools/abr/output`

Reads measurement data from `c:/data/metingen.xlsx`, sheet 'final' and column 'sample_name'. Performs double-sided testing and generates summary and top-3 content for both habitat-types and plant-species and results in report `c:/tools/abr/output/Report_TESTRUN_20210823.xlsx`

To blacklist Habitattypes `H1310b`, `H1330a`, `H2160` and `H2170`, provide a blacklist file (e.g. `c:\documenten\selectie_habitattypen.csv`) formatted as:

|ID|decision|
|----|----|
|H1310b|yes|
|H1330a|yes|
|H1330b|no|
|H2120|no|
|H2130a|no|
|H2130b|no|
|H2130c|no|
|H2140a|no|
|H2140b|no|
|H2150|no|
|H2160|yes|
|H2170|yes|

and use the blacklisting parametergroup as:

```
--blacklist --bl_io c:\documenten\selectie_habitattypen.csv --bl_id ID --bl_sep --bl_filter_col decision --bl_true_val yes --bl_type SpecNr  
```

## CONTACT
* Wieger Wamelink (content and management ABR database)
* Hans Roelofsen (ABR tool)

## Copyright
Wageningen Environmental Research 2021. 
